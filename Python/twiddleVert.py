#!/usr/bin/python

import sys
import time
import math as m
import ArduinoControl as ctrl
import DataLog

dl = DataLog.valueHandler('Power', 'Damp', 'Best', 'Sumstep')

gain      = [0.0, 0.0]
step      = [0.1, 0.01]
tolerance = 0.00001
bestError = 0.0

gain = ctrl.getBalanceGains()                                      #Maybe also position gains
print gain
'''
for t in range (len(gain)):
    gain[t] -= step[t]
print gain
'''

print(ctrl.getRobotState())
while ctrl.getRobotState() != "U":
    ctrl.getRobotState()
print "Robot  Up"
time.sleep(1)                                                      #Wait until bot is steadingly balancing

def getError():
    ctrl.setBalanceGains(gain[0], gain[1])
    (speed, angle) = ctrl.getBalanceError()
    absAngle = abs(angle)
    return absAngle

bestError = getError()

while sum(step) > tolerance:
    for t in range (len(gain)):
        gain[t] += step[t]
        error = getError()
        if error < bestError:
            bestError = error
            step[t] *= 1.1 
        else:
            gain[t] -= 2 * step[t] 
            error = getError()
            if error < bestError:
                bestError = error
                step[t] *= 1.1
            else:
                gain[t] += step[t]
                step[t] *= 0.9
    dl.keepValues(gain[0], gain[1], bestError, sum(step))
print "Finished Twiddle"
dl.writeValues()
dl.printValues()

