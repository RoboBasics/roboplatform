#!/usr/bin/python

import serial
import threading
import time

#ser = serial.Serial('/dev/serial0', 9600)    # RPi 3
ser = serial.Serial('/dev/ttyACM0', 921600)   # RPi 2
print "Serial port opened"

class globals(object): pass
g = globals()
g.CONNECTED = False

def connected():
    return g.CONNECTED

class Connection ( threading.Thread ):

    def __init__( self, comms, reply, lidar, error ):
        threading.Thread.__init__( self )
        self.stopEvent = threading.Event()
        self.comms  = comms
        self.reply = reply
        self.lidar  = lidar
        self.error = error
        self.marker = '\n'

    def stop( self ):
        self.stopEvent.set()

    def isStopped( self ):
        return self.stopEvent.is_set()

    def run( self ):
        while not self.isStopped():
            if not self.comms.empty():
                q = self.comms.get()
                opcodeOut  = q[0]
                opvalueOut = q[1]
                stringValueOut = str(opvalueOut)
                messageOut = opcodeOut + stringValueOut + self.marker
                ser.write(messageOut)
                self.comms.task_done()
            self.getMessageFromArduino()

    def getMessageFromArduino(self):
        if  ser.inWaiting() > 1:
            opCodeIn = ser.read()
            messageIn = "" 
            charIn = ser.read()
            while charIn != self.marker:
                messageIn += charIn
                charIn = ser.read()
            if opCodeIn == 'C':
                g.CONNECTED = True
            elif opCodeIn == 'l':
                self.lidar.put(messageIn)
            elif opCodeIn == 'j':
                self.error.put(messageIn)
            else: 
                self.reply.put(messageIn)

    def useInteger(self, messageIn):
        try:
            test = int(messageIn)
            return test
        except ValueError:
            return 9999

    def useFloat(self, messageIn):
        try:
            test = float(messageIn)
            return test
        except ValueError:
            return 9999.9
    
    def serialOpened(self):
        return ser.isOpen()


