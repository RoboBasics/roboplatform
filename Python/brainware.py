#!/usr/bin/python

import sys
import time
import math as m
import ArduinoControl as ctrl
import dataLogger   

class globals(object): pass                                                      # Create a 'struct' for global variables
g = globals()

g.lidarTimer = time.clock()
g.observations = {}
g.Landmarks = {}
g.keyList = []
g.inserted = 0
g.updated = 0
g.removed = 0
g.dbStatus = [0,0]


mv = measurements.valueHandler('X','Y')
lm = measurements.valueHandler('X','Y')
rm = measurements.valueHandler('a','d')

def mainExit():
    ctrl.stopLidar()
    time.sleep(1)
    ctrl.stopThreading()
    sys.exit() 

def showDbStatus():
    print "Added     = ",g.dbStatus[0]
    print "Confirmed = ",g.dbStatus[1]
    print "Members   = ",len(g.observations)
    print "Landmarks = ",len(g.Landmarks)
    print "================"
    
def processLidarReadings():
    'get new lidar coordinates, transpose to new position & update observations'

    # Hier moet de robotpositie opgehaald worden om de nodecoordinaten op aan te passen

    while ctrl.lidarUpdateAvailable():
        angle, range = ctrl.getLidarUpdate()
        rm.keepValues(angle,range)
        x = range * (m.cos(m.radians(angle)))
        x = round(x,0)
        X = int(x)
        y = range * (m.sin(m.radians(angle)))
        y = round(y,0)
        Y = int(y)

        # Node aanpassen aan robotpositie

        node = [X,Y]

        if g.observations.has_key(str(node)):
            observed = g.observations[str(node)] + 1
            g.observations[str(node)] = observed
            g.dbStatus[1] +=1
            updateLandmarks()
        else:
            g.observations[str(node)] = 1 
            g.dbStatus[0] +=1
            mv.keepValues(X, Y)

def updateLandmarks():
    g.keyList = g.observations.keys()
    for key in g.keyList:
        if g.observations[key] > 2:
            if not g.Landmarks.has_key(key):
                g.Landmarks[key] = 1

if __name__ == "__main__":
    ctrl.startLidar()
    time.sleep(1)
    
    while True:
        try:
            if time.clock() - g.lidarTimer > 0.02:
                processLidarReadings()

                showDbStatus()
 
                g.lidarTimer = time.clock()

        except KeyboardInterrupt:
            g.keyList = g.observations.keys()
            g.keyList.sort()
            for key in g.keyList:
                if g.observations[key] > 2:
                    print key, g.observations[key]

            g.keyList = g.Landmarks.keys()
            for key in g.keyList:
                (lmx,lmy) = map(int, key.strip()[1:-1].split(','))
                lm.keepValues(lmx,lmy)

            mv.writeValues()
            time.sleep(5)
            lm.writeValues()
            time.sleep(5)
            rm.writeValues()
            ctrl.stopLidar()
            mainExit()
