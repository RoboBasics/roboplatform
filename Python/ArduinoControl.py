#!/usr/bin/python

import time
import math
import threading
import Queue
import ArduinoConnection as ac

comms = Queue.Queue()
reply = Queue.Queue()
lidar = Queue.Queue()
error = Queue.Queue()

bus = ac.Connection(comms, reply, lidar, error)
bus.start()
print "Bus thread started"
print "IMU recalibration"

time.sleep(20)  #Need a waiting loop for IMU calibration (Arduino restarts when a serial port is opened)

while not bus.serialOpened():
	continue

code = 'c'
cmd = [code, 0]
comms.put(cmd)
print "Arduino hailed"

while not ac.connected():
    continue
print "Arduino connected"	

def getBalanceGains():
    gains = [0.0]*2
    code = 'a'
    cmd = [code, 0]
    comms.put(cmd)
    with reply.mutex:
        reply.queue.clear()
    upd = reply.get()
    (gains[0],gains[1]) = map(float, upd.strip()[1:-1].split(','))
    reply.task_done()
    return gains

def setBalanceGains(gainP, gainD):
    code = 'A'
    cmd = [code, gainP]
    comms.put(cmd)
    code = 'B'
    cmd = [code, gainD]
    comms.put(cmd)

def getBalanceError():
    upd = error.get()
    (speed, angle) = map(float, upd.strip()[1:-1].split(','))
    with error.mutex:
        error.queue.clear()
    error.task_done()
    return speed, angle

def getRobotState():
    code = 'k'
    cmd = [code, 0]
    comms.put(cmd)
    with reply.mutex:
        reply.queue.clear()
    resp = reply.get()
    reply.task_done()
    return resp
 
def startLidar():
    code = 'o'
    cmd = [code, 0]
    comms.put(cmd)

def stopLidar():
    code = 'O'
    cmd = [code, 0]
    comms.put(cmd)

def lidarUpdateAvailable():
    return not lidar.empty()

def replyAvailable():
    return not reply.empty()

def getLidarUpdate():
    upd = lidar.get()
    (ang,rng) = map(int, upd.strip()[1:-1].split(','))
    return ang, rng

def stopThreading():
	bus.stop()
