#!/usr/bin/python

import sys
import time
import math as m
import ArduinoControl as ctrl
import DataLog

class globals(object): pass                                                      # Create a 'struct' for global variables
g = globals()
g.gainVert      = [0.0, 0.0]
g.gainHor       = [0.0, 0.0]
g.stepVert      = [0.1, 0.01]
g.stepHor       = [0.1, 0.01]
g.bestErrorVert = 0.0
g.bestErrorHor  = 0.0
g.tolerance     = 0.00001

dl = DataLog.valueHandler('PowerH', 'DampH', 'BestH', 'SumstepH', 'PowerV', 'DampV', 'BestV', 'SumstepV')

def mainExit():
    ctrl.stopLidar()
    time.sleep(1)
    ctrl.stopThreading()
    sys.exit() 

def getError():
    ctrl.setBalanceGains(g.gainVert[0], g.gainVert[1])
    (speed, angle) = ctrl.getBalanceError()
    absSpeed = abs(speed)
    absAngle = abs(angle)
    return absSpeed, absAngle

def twiddleVert():
    g.stepVert = [0.1, 0.01]
    __, g.bestErrorVert = getError()

    while sum(g.stepVert) > g.tolerance:
        for t in range (len(g.gainVert)):
            g.gainVert[t] += g.stepVert[t]
            __, error = getError()
            if error < g.bestErrorVert:
                g.bestErrorVert = error
                g.stepVert[t] *= 1.1 
            else:
                g.gainVert[t] -= 2 * g.stepVert[t] 
                __, error = getError()
                if error < g.bestErrorVert:
                    g.bestErrorVert = error
                    g.stepVert[t] *= 1.1
                else:
                    g.gainVert[t] += g.stepVert[t]
                    g.stepVert[t] *= 0.9
        #dl.keepValues(g.gainVert[0], g.gainVert[1], g.bestErrorVert, sum(g.stepVert))
    #print "Finished vertical twiddle"



gain = ctrl.getBalanceGains()                                      #Maybe also position gains
print (gain)
'''
for t in range (len(gain)):
    gain[t] -= step[t]
print gain
'''

print(ctrl.getRobotState())
while ctrl.getRobotState() != "U":
    ctrl.getRobotState()
print ("Robot  Up")
time.sleep(1)                                                      #Wait until bot is steadingly balancing


bestError = getError()

while sum(step) > tolerance:
    for t in range (len(gain)):
        gain[t] += step[t]
        error = getError()
        if error < bestError:
            bestError = error
            step[t] *= 1.1 
        else:
            gain[t] -= 2 * step[t] 
            error = getError()
            if error < bestError:
                bestError = error
                step[t] *= 1.1
            else:
                gain[t] += step[t]
                step[t] *= 0.9
    dl.keepValues(gain[0], gain[1], bestError, sum(step))
print ("Finished Twiddle")
dl.writeValues()
dl.printValues()

